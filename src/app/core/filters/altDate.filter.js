(function ()
{
    'use strict';

    angular
        .module('app.core')
        .filter('altDate', altDate);

    /** @ngInject */
    function altDate()
    {
        return function (value)
        {
            // var value = value.substr(0, value.length-7); // only for local changes
            return moment(value).format('hh:mm a, DD MMM YYYY');
        };
    }

})();