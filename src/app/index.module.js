(function ()
{
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
        .module('fuse', [
            'ngCsvImport',
            'ngTableToCsv',
            'google.places',
            'ngMap',
            // Core
            'app.core',
            // Navigation
            'app.navigation',

            // Toolbar
            'app.toolbar',

            // Quick Panel
            'app.quick-panel',

            // Sample
            'app.pages.auth.login',
            'app.pages.auth.register',
            'app.chat',
            'app.morenet'
        ]);
})();