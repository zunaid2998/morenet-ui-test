(function ()
{
    'use strict';

    angular
        .module('app.morenet')
        .filter('customDateFormat', function(){
            return function(date){
                return date ? date.split(" ")[0] : null;
            }
        })
        .controller('MorenetController', MorenetController);

    /** @ngInject */
    function MorenetController($state, $cookies, $mdDialog, $http, $rootScope, $scope, $stateParams, $window, $timeout, $document, $mdToast, $mdSidenav){
        
        var vm = this;
        vm.base_api_url = $rootScope.base_api_url;
        
        
        // Data
        vm.templateDashboardUrl = 'app/main/morenet/dashboard.html';
        vm.incomingMessagesUrl = 'app/main/morenet/incoming-messages.html';
        vm.helpUrl = 'app/main/morenet/help.html';
        vm.groupAdminTable = 'app/main/morenet/group-admin-table.html';
        vm.groupTable = 'app/main/morenet/group-table.html';
        vm.overviewPage = 'app/main/morenet/overview.html';
        vm.importImgUrl = 'https://thegoldenshop.net/wp-content/uploads/2018/02/column_names.png';

        // morenet pages
        vm.adminsPage = 'app/main/morenet/admins-page.html';
        vm.contractorsPage = 'app/main/morenet/contractors-page.html';
        vm.installersPage = 'app/main/morenet/installers-page.html';
        vm.customersPage = 'app/main/morenet/customers-page.html';
        vm.notificationsPage  = 'app/main/morenet/notifications-page.html';

        // morenet pages end

        vm.incomingMessagesHeight = $window.innerHeight - 108 + 'px !important';
        vm.groups = [];
        vm.user = $stateParams.user;


        vm.admins = [];
        vm.contractors = [];
        vm.lightContractors = [];
        vm.installers = [];
        vm.lightInstallers = [];
        vm.customers = [];
        vm.notifications = [];
        vm.allCustomers = [];
        vm.logoutClicked = logoutClicked;
        vm.openAdminModal = openAdminModal;

        // morenet methods
        
        vm.initMorenet = initMorenet;
        vm.navigateTo = navigateTo;
        vm.navigation = {
            dashboard : false,
            admins : false,
            contractors : false,
            installers : false,
            customers : false,
            notifications: false
        };

        // will be deleting later on - only for dev purpose
        /*vm.user = {
            user_role_id: 1
        }*/

        vm.fetchAdmins = fetchAdmins;
        vm.fetchContractors = fetchContractors;
        vm.fetchInstallers = fetchInstallers;
        vm.fetchCustomers = fetchCustomers;
        vm.fetchNotifications = fetchNotifications;
        vm.fetchInstallersByContractorId = fetchInstallersByContractorId;
        vm.fetchCustomersByContractorId = fetchCustomersByContractorId;
        vm.fetchCustomersByInstallerId = fetchCustomersByInstallerId;
        vm.fetchNotificationsByContractorId = fetchNotificationsByContractorId;
        vm.fetchNotificationsByInstallerId = fetchNotificationsByInstallerId;        
        vm.onSelectCustomerFilter = onSelectCustomerFilter;
        vm.filterCustomerByContractor = filterCustomerByContractor;
        vm.filterCustomerByInstaller = filterCustomerByInstaller;
        vm.getInstallationStatusDesc = getInstallationStatusDesc;
        vm.getInstallationTypeDesc = getInstallationTypeDesc;
        vm.getPostProcessingStatusDesc = getPostProcessingStatusDesc;
        vm.markNotifciationAsRead = markNotifciationAsRead;
        vm.markAllNotifciationAsRead = markAllNotifciationAsRead;
        vm.openCustomerModalFromMap = openCustomerModalFromMap;


        function openCustomerModalFromMap(evt, customer){
            if(customer){
                vm.openCustomerModal(evt, customer);
            }
        }

        function markAllNotifciationAsRead(){
            
        }

        function markNotifciationAsRead(notification){
            $http({
                url: vm.base_api_url + '/api/notifications/mark-as-read',
                method: "PUT",
                data: {
                    "id": notification.id
                }
            }).then(function successCallback(response){
               // console.log(response);
                /*if(response.data){
                    $rootScope.$broadcast('contractorUpdated', response.data);
                }*/
            }, function errorCallback(response){});
        }

        function getPostProcessingStatusDesc(post_processing_status_id){
            if(post_processing_status_id === 1){
                return "Not started";
            } else if(post_processing_status_id === 2){
                return "Completed";
            } else if(post_processing_status_id === 3){
                return "Failed";
            }
        }

        function getInstallationStatusDesc(installation_status_id){
            if(installation_status_id === 1){
                return "Created";
            } else if(installation_status_id === 2){
                return "Finished";
            } else if(installation_status_id === 3){
                return "Abandoned";
            }
        }

        function getInstallationTypeDesc(installation_type_id){
            return installation_type_id === 1 ? 'Standard' : 'Commercial';
        }
        
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = false;

        var pusher = new Pusher('1336cecf96734ec47205', {
        cluster: 'us2',
        forceTLS: true
        });



        var channel = pusher.subscribe('customer-channel');
        channel.bind('status-event', function(data) {

            
            // var filterUser = [
            //     {'pk' : 'super_admin_id', 'user_role' : 1},
            //     {'pk' : 'admin_id', 'user_role' : 2},
            //     {'pk' : 'contractor_id', 'user_role' : 3},
            //     {'pk' : 'installer_id', 'user_role' : 4},
            // ]
            

            // for Ahmed:
            // filter for checking if user exists inside data
            // if user comes in the notification data, then show badge with new notification
            // also call method "fetcNotifications"


            angular.forEach(data.data, function(user) {
               // console.log('log user:', user)
            })
            
            
        });        

        if(vm.user && vm.user.user_role_id === 1){
            vm.fetchAdmins();
            vm.fetchContractors();
            vm.fetchInstallers();
            vm.fetchCustomers();            
        }
        if(vm.user && vm.user.user_role_id === 2){
            vm.fetchContractors();
            vm.fetchInstallers();
            vm.fetchCustomers();            
        }
        if(vm.user && vm.user.user_role_id === 3){
            vm.fetchInstallersByContractorId(vm.user.contractor_id);
            vm.fetchCustomersByContractorId(vm.user.contractor_id);                        
        }
        if(vm.user && vm.user.user_role_id === 4){
            vm.fetchCustomersByInstallerId(vm.user.installer_id);
        }        

        vm.fetchNotifications();

        function fetchInstallersByContractorId(contractor_id){
            $http({
                url: vm.base_api_url + '/api/contractor/'+ contractor_id +'/installers',
                method: "GET"
            }).then(function successCallback(response){
                    var installers = response.data.installers;
                    var lightContractors = response.data.lightContractors;
                    angular.forEach(installers, function(installer){
                        installer.contractor_name = getContractorNameById(lightContractors, installer.contractor_id);
                    });
                    vm.installers = installers;
                    vm.lightContractors = lightContractors;
            }, function errorCallback(response){});
        }

        function fetchCustomersByContractorId(contractor_id){
            $http({
                url: vm.base_api_url + '/api/contractor/'+ contractor_id+'/customers',
                method: "GET"
            }).then(function successCallback(response){
                    var customers = response.data.customers;
                    var lightInstallers = response.data.lightInstallers;
                    var lightContractors = response.data.lightContractors;
                    vm.customers = customers;
                    vm.allCustomers = customers;
                    vm.lightInstallers = lightInstallers;
                    vm.lightContractors = lightContractors;
            }, function errorCallback(response){});
        }

        function fetchCustomersByInstallerId(installer_id){
            $http({
                url: vm.base_api_url + '/api/installer/'+ installer_id +'/customers',
                method: "GET"
            }).then(function successCallback(response){
                    var customers = response.data.customers;
                    var lightInstallers = response.data.lightInstallers;
                    var lightContractors = response.data.lightContractors;
                    vm.customers = customers;
                    vm.allCustomers = customers;
                    vm.lightInstallers = lightInstallers;
                    vm.lightContractors = lightContractors;
            }, function errorCallback(response){});
        }

        function fetchNotificationsByContractorId(installer_id) {

            //console.log('implement fetching fetchNotificationsByContractorId')

        }



        function fetchNotificationsByInstallerId(installer_id) {

            //console.log('implement fetching fetchNotificationsByInstallerId')

        }
        

        function fetchAdmins(){
            $http({
                url: vm.base_api_url + '/api/superAdmin/admins',
                method: "GET"
            }).then(function successCallback(response){
                    vm.admins = response.data;
                   // console.log(response.data);
            }, function errorCallback(response){});
        }
        function fetchContractors(){
            $http({
                url: vm.base_api_url + '/api/contractors',
                method: "GET"
            }).then(function successCallback(response){
                    vm.contractors = response.data;
                   // console.log(response.data);
            }, function errorCallback(response){});
        }
        function fetchInstallers(){
            $http({
                url: vm.base_api_url + '/api/installers',
                method: "GET"
            }).then(function successCallback(response){
                    var installers = response.data.installers;
                    var lightContractors = response.data.lightContractors;
                    var contractorsWithInstallers = response.data.contractorsWithInstallers;
                    vm.installers = installers;
                    vm.lightContractors = lightContractors;
                    vm.contractorsWithInstallers = contractorsWithInstallers;
            }, function errorCallback(response){});
        }

        function getContractorNameById(lightContractors, contractor_id){
            var contractor_name = null;
            angular.forEach(lightContractors, function(contractor){
                if(contractor.contractor_id == contractor_id){
                    contractor_name = contractor.contractor_name;
                    return contractor_name;
                }
            });
            return contractor_name;
        }

        function getInstallerNameById(lightInstallers, installer_id){
            var installer_name;
            angular.forEach(lightInstallers, function(installer){
                if(installer.installer_id == installer_id){
                    installer_name = installer.installer_name;
                }
            });
            return installer_name;
        }

        function fetchCustomers(){
            $http({
                url: vm.base_api_url + '/api/customers',
                method: "GET"
            }).then(function successCallback(response){
                    var customers = response.data.customers;
                    var lightInstallers = response.data.lightInstallers;
                    var lightContractors = response.data.lightContractors;
                    var contractorsWithInstallers = response.data.contractorsWithInstallers;
                    vm.customers = customers;
                    vm.allCustomers = customers;
                    vm.lightInstallers = lightInstallers;
                    vm.lightContractors =lightContractors;
                    vm.contractorsWithInstallers = contractorsWithInstallers;
            }, function errorCallback(response){});
        }

        function fetchNotifications() {
            if(vm.user && vm.user.user_role_id === 1) return;

            var notificationUrl;            
            var notificationsApiUrl = [
                {'user_role_id' : 2, 'url' : '/api/admin/notifications/' + vm.user.admin_id},
                {'user_role_id' : 3, 'url' : '/api/contractor/notifications/' + vm.user.contractor_id},
                {'user_role_id' : 4, 'url' : '/api/installer/notifications/' + vm.user.installer_id},
            ];
            
            angular.forEach(notificationsApiUrl, function(filter) {
                
                if (vm.user.user_role_id === filter.user_role_id) {

                    notificationUrl =  filter.url
                }

            })

            if (notificationUrl !== null) {
                $http({
                    url: vm.base_api_url + notificationUrl,
                    method: "GET"
                }).then(function successCallback(response){
                    var notifications = response.data.data;
                    var allNotifications = [];
                    if(notifications.length > 0){
                        notifications.forEach(function(notification){
                            allNotifications.push({
                                id: notification.id,
                                customer_name: notification.data.customer_name,
                                customer_id: notification.data.customer_id,
                                message: notification.data.message,
                                author_name: notification.data.author_name || "Superadmin"
                            });
                        })
                    }
                    vm.notifications = allNotifications;
                }, function errorCallback(response){});                
                
            } else {
                console.trace('couldnt fetch notifications.');
            }    
        }        

        function onSelectCustomerFilter(){
            if(vm.selectedCustomerFilter){
                vm.customers = vm.allCustomers.filter(function(customer){
                    return customer.installation_status_id == vm.selectedCustomerFilter;
                });
            } else {
                vm.customers = vm.allCustomers;
            }
        }

        function filterCustomerByContractor(){
            if(vm.selectedContractor){
                vm.customers = vm.allCustomers.filter(function(customer){
                    return customer.contractor_id == vm.selectedContractor;
                });
            } else {
                vm.customers = vm.allCustomers;
            }
        }

        function filterCustomerByInstaller(){
            if(vm.selectedInstaller){
                vm.customers = vm.allCustomers.filter(function(customer){
                    return customer.installer_id == vm.selectedInstaller;
                });
            } else {
                vm.customers = vm.allCustomers;
            }
        }

        function navigateTo(component){
            var navigation = vm.navigation;
            for (var prop in navigation) {
                if(navigation.hasOwnProperty(prop) && prop === component){
                    navigation[prop] = true;
                } else {
                    navigation[prop] = false;
                }
            }
        }

        function initMorenet()
        {
            vm.navigation.customers = true;
        }

        vm.initMorenet();

        // morenet methods finished here

         // Admin scopes - that need to be updated
         $scope.$on('adminAdded', function(evt, data){
            vm.admins = data;
        });
        $scope.$on('adminUpdated', function(evt, data){
            angular.forEach(vm.admins, function(admin) {
                if(admin.admin_id == data.admin_id){
                    admin.first_name = data.first_name;
                    admin.last_name = data.last_name;
                    admin.phone_office = data.phone_office;
                    admin.phone_cell = data.phone_cell;
                    admin.email = data.email;
                    admin.password = data.password;
                    admin.retype_password = data.password;
                    admin.department = data.department;
                    admin.status = data.status;
                }
            })
        });

        // Contractor Scopes - that need to be updates
        $scope.$on('contractorAdded', function(evt, data){
            vm.contractors = data;
            if(vm.user && vm.user.user_role_id === 1 || vm.user && vm.user.user_role_id === 2){
                vm.fetchInstallers();
            }
            if(vm.user && vm.user.user_role_id === 3){
                vm.fetchInstallersByContractorId(vm.user.contractor_id);
            }
        });
        $scope.$on('contractorUpdated', function(evt, data){
            angular.forEach(vm.contractors, function(contractor) {
                if(contractor.contractor_id == data.contractor_id){
                    contractor.company = data.company;
                    contractor.contact_name = data.contact_name;
                    contractor.phone_office = data.phone_office;
                    contractor.phone_cell = data.phone_cell;
                    contractor.email_sales = data.email_sales;
                    contractor.email_support = data.email_support;
                    contractor.password = data.password;
                    contractor.retype_password = data.password,
                    contractor.address = data.address,
                    contractor.website = data.website;
                    contractor.status = data.status;
                }
            })
        });

        // Installer Scopes - that need to be updates
        $scope.$on('installerAdded', function(evt, response){
            var installers = response.data.installers;
            var lightContractors = response.data.lightContractors;
            angular.forEach(installers, function(installer){
                installer.contractor_name = getContractorNameById(lightContractors, installer.contractor_id);
            });

            if(vm.user && vm.user.user_role_id === 3){
                vm.fetchInstallersByContractorId(vm.user.contractor_id);
                vm.fetchCustomersByContractorId(vm.user.contractor_id);
            } else {
                vm.fetchCustomers();
                vm.installers = installers;
                vm.lightContractors = lightContractors;
            }
        });
        $scope.$on('installerUpdated', function(evt, data){
            vm.installers = data.installers;
            vm.lightContractors = data.lightContractors;
            vm.contractorsWithInstallers = data.contractorsWithInstallers;
        });

        // Customer Scopes - that need to be updates
        $scope.$on('customerAdded', function(evt, response){
            var customers = response.data.customers;
            var lightInstallers = response.data.lightInstallers;
            angular.forEach(customers, function(customer){
                customer.installer_name = getInstallerNameById(lightInstallers, customer.installer_id);
            });

            if(vm.user && vm.user.user_role_id === 3){
                vm.fetchCustomersByContractorId(vm.user.contractor_id);
            }else if(vm.user && vm.user.user_role_id === 4){
                vm.fetchCustomersByInstallerId(vm.user.installer_id);
            } else {
                vm.fetchCustomers();
                vm.customers = customers;
                vm.lightInstallers = lightInstallers;
            }
            vm.customers = customers;
            vm.lightInstallers = lightInstallers;
        });
        $scope.$on('customerUpdated', function(evt, data){
            vm.customers = data.customers;
        });

        function logoutClicked(){
            $cookies.put('userLoggedIn', 'false');
            $state.go('app.pages_auth_login');
        }

        
        function openAdminModal(ev, admin){
            if(admin){
                var admin = {
                    admin_id: admin.admin_id,
                    first_name: admin.first_name,
                    last_name: admin.last_name,
                    phone_office: admin.phone_office,
                    phone_cell: admin.phone_cell,
                    email: admin.email,
                    password: admin.password,
                    retype_password: admin.password,
                    department: admin.department,
                    status: admin.status ? true : false,
                    username: admin.username
                };
                vm.admin = admin;
            }
            $mdDialog.show({
                locals: {admin: admin},
                controller: AdminDialogController,
                controllerAs: 'vm',
                templateUrl: 'app/main/morenet/admin-dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: false
              });
        }
        vm.openContractorModal = openContractorModal;
        function openContractorModal(ev, contractor){
            if(contractor){
                var contractor = {
                    contractor_id: contractor.contractor_id,
                    company: contractor.company,
                    contact_name: contractor.contact_name,
                    phone_office: contractor.phone_office,
                    phone_cell: contractor.phone_cell,
                    email_sales: contractor.email_sales,
                    email_support: contractor.email_support,
                    password: contractor.password,
                    retype_password: contractor.password,
                    address: contractor.address,
                    website: contractor.website,
                    status: contractor.status ? true : false,
                    username: contractor.username
                };
                vm.contractor = contractor;
            }
            $mdDialog.show({
                locals: {contractor: contractor},
                controller: ContractorDialogController,
                controllerAs: 'vm',
                templateUrl: 'app/main/morenet/contractor-dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: false
              });
        }

        vm.openInstallerModal = openInstallerModal;
        function openInstallerModal(ev, installer){
            if(installer){
                var installer = {
                    installer_id: installer.installer_id,
                    first_name: installer.first_name,
                    last_name: installer.last_name,
                    phone_office: installer.phone_office,
                    phone_cell: installer.phone_cell,
                    email: installer.email,
                    password: installer.password,
                    retype_password: installer.password,
                    address: installer.address,
                    status: installer.status ? true : false,
                    contractor_id: installer.contractor_id,
                    username: installer.username
                };
                vm.installer = installer;
            }
            $mdDialog.show({
                locals: {installer: installer, lightContractors: vm.lightContractors, user: vm.user},
                controller: InstallerDialogController,
                controllerAs: 'vm',
                templateUrl: 'app/main/morenet/installer-dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: false
              });
        }

        vm.openCustomerModal = openCustomerModal;
        vm.openCustomerModalFromNotification = openCustomerModalFromNotification;
        function openCustomerModalFromNotification(ev, customer_id){
            var customer = vm.customers.find(function(x){
                return x.customer_id === customer_id;
            });
            this.openCustomerModal(ev, customer);

        }
        function openCustomerModal(ev, customer){
            if(customer){
                var customer = {
                    customer_id: customer.customer_id,
                    first_name: customer.first_name,
                    last_name: customer.last_name,
                    phone_home: customer.phone_home,
                    phone_cell: customer.phone_cell,
                    email: customer.email,
                    address: customer.address,
                    unit_no: customer.unit_no,
                    installation_date: customer.installation_date,
                    radio_unit_no: customer.radio_unit_no,
                    router_unit_no: customer.router_unit_no,
                    elevation: customer.elevation,
                    longitude: customer.longitude,
                    latitude: customer.latitude,
                    status: customer.status ? true : false,
                    installer_id: customer.installer_id,
                    contractor_id: customer.contractor_id,
                    installation_status_id: customer.installation_status_id,
                    installation_type_id: customer.installation_type_id,
                    company_name: customer.company_name,
                    signal_quality: customer.signal_quality,
                    account_number: customer.account_number,
                    post_processing_status_id: customer.post_processing_status_id,
                    post_processing_note: customer.post_processing_note,
                    address_lat: customer.address_lat,
                    address_long: customer.address_long,
                    note: customer.note,
                    read_only: customer.read_only || false
                };
                vm.customer = customer;
            }
            $mdDialog.show({
                locals: {customer: customer,
                         allCustomers: vm.allCustomers, 
                         lightInstallers: vm.lightInstallers, 
                         user: vm.user, 
                         lightContractors: vm.lightContractors,
                         contractorsWithInstallers: vm.contractorsWithInstallers},
                controller: CustomerDialogController,
                controllerAs: 'vm',
                templateUrl: 'app/main/morenet/customer-dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: false
              });
        }
    }

    function ContractorDialogController($scope, $mdDialog, $http, $mdToast, $rootScope, contractor) {
        var vm = this;
        vm.contractor = contractor;
        vm.base_api_url = $rootScope.base_api_url;
       
        // data
        vm.newContractor = contractor ? false : true;
        //vm.contractors = [];
        vm.contractorDialogHeader = contractor ? "Edit" : "New Contractor";
        
        // method
        vm.addNewContractor = addNewContractor;
        vm.saveContractor = saveContractor;
        vm.closeDialog = closeDialog;
        vm.onChangeName = onChangeName;

        // method implementation
        function onChangeName(){
            vm.contractor.username = vm.contractor.contact_name.split(" ").join('').toLowerCase();
        }
        function addNewContractor(){
            if(vm.contractor.password === vm.contractor.retype_password){
                $http({
                    url: vm.base_api_url + '/api/contractors/create',
                    method: "POST",
                    data: {
                        "company": vm.contractor.company,
                        "contact_name": vm.contractor.contact_name,
                        "phone_office": vm.contractor.phone_office,
                        "phone_cell": vm.contractor.phone_cell,
                        "email_sales": vm.contractor.email_sales,
                        "email_support": vm.contractor.email_support,
                        "password": vm.contractor.password,
                        "address": vm.contractor.address,
                        "website": vm.contractor.website,
                        "status": vm.contractor.status,
                        "username": vm.contractor.username
                    }
                }).then(function successCallback(response){
                    if(response){
                        if(response.data === "is_existing_user"){
                            vm.isExistingUser = true;
                        } else {
                            $rootScope.$broadcast('contractorAdded', response.data);
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('Contractor Added Successfully')
                                    .position('bottom right')
                                    .hideDelay(3000)                          
                            );
                            $mdDialog.hide();
                        }
                    }
                    
                }, function errorCallback(response){});
            } else {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Retype password is not matching with password! Pleast try again')
                        .position('bottom right')
                        .hideDelay(3000)                          
                );
            }
        }
        function saveContractor(contractor){
            if(vm.contractor.password === vm.contractor.retype_password){
                $http({
                    url: vm.base_api_url + '/api/contractors/'+ vm.contractor.contractor_id,
                    method: "PUT",
                    data: {
                        "company": vm.contractor.company,
                        "contact_name": vm.contractor.contact_name,
                        "phone_office": vm.contractor.phone_office,
                        "phone_cell": vm.contractor.phone_cell,
                        "email_sales": vm.contractor.email_sales,
                        "email_support": vm.contractor.email_support,
                        "password": vm.contractor.password,
                        "address": vm.contractor.address,
                        "website": vm.contractor.website,
                        "status": vm.contractor.status
                    }
                }).then(function successCallback(response){
                    if(response.data){
                        $rootScope.$broadcast('contractorUpdated', response.data);
                    }
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Contractor updated Successfully')
                            .position('bottom right')
                            .hideDelay(3000)                          
                    );
                    vm.closeDialog();
                }, function errorCallback(response){});
            } else {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Retype password is not matching with password! Pleast try again')
                        .position('bottom right')
                        .hideDelay(3000)                          
                );
            }
        }
        function closeDialog(){
            $mdDialog.hide();
        }
    }

    function InstallerDialogController($scope, $mdDialog, $http, $mdToast, $rootScope, installer, lightContractors, user) {
        var vm = this;
        vm.user = user;
        vm.installer = installer;
        vm.lightContractors = lightContractors;
        vm.base_api_url = $rootScope.base_api_url;
       
        // data
        vm.newInstaller = installer ? false : true;
        //vm.contractors = [];
        vm.installerDialogHeader = installer ? "Edit" : "New Installer";
        
        // method
        vm.addNewInstaller = addNewInstaller;
        vm.saveInstaller = saveInstaller;
        vm.closeDialog = closeDialog;
        vm.onChangeFirstName = onChangeFirstName;
        vm.onChangeLastName = onChangeLastName;

        // method implementation
        function onChangeFirstName(){
            vm.installer.username = vm.installer.first_name.toString().toLowerCase().trim().charAt(0);
        }
        function onChangeLastName(){
            vm.installer.username = vm.installer.first_name.toString().toLowerCase().trim().charAt(0) + vm.installer.last_name.toString().toLowerCase().trim();
        }
        function addNewInstaller(){
            if(vm.installer.password === vm.installer.retype_password){
                $http({
                    url: vm.base_api_url + '/api/installers/create',
                    method: "POST",
                    data: {
                        "first_name": vm.installer.first_name,
                        "last_name": vm.installer.last_name,
                        "phone_office": vm.installer.phone_office,
                        "phone_cell": vm.installer.phone_cell,
                        "email": vm.installer.email,
                        "password": vm.installer.password,
                        "address": vm.installer.address,
                        "status": vm.installer.status,
                        "contractor_id": vm.installer.contractor_id || vm.user.contractor_id,
                        "username": vm.installer.username
                    }
                }).then(function successCallback(response){
                    if(response){
                        if(response.data === "is_existing_user"){
                            vm.isExistingUser = true;
                        } else {
                            $rootScope.$broadcast('installerAdded', response);
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('Installer Added Successfully')
                                    .position('bottom right')
                                    .hideDelay(3000)                          
                            );
                            $mdDialog.hide();
                        }
                    }
                }, function errorCallback(response){});
            } else {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Retype password is not matching with password! Pleast try again')
                        .position('bottom right')
                        .hideDelay(3000)                          
                );
            }
        }
        function saveInstaller(installer){
            if(vm.installer.password === vm.installer.retype_password){
                $http({
                    url: vm.base_api_url + '/api/installers/'+ vm.installer.installer_id,
                    method: "PUT",
                    data: {
                        "first_name": installer.first_name,
                        "last_name": installer.last_name,
                        "phone_office": installer.phone_office,
                        "phone_cell": installer.phone_cell,
                        "email": installer.email,
                        "password": installer.password,
                        "address": installer.address,
                        "status": installer.status,
                        "contractor_id": installer.contractor_id
                    }
                }).then(function successCallback(response){
                    if(response.data){
                        $rootScope.$broadcast('installerUpdated', response.data);
                    }
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Installer updated Successfully')
                            .position('bottom right')
                            .hideDelay(3000)                          
                    );
                    vm.closeDialog();
                }, function errorCallback(response){});
            } else {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Retype password is not matching with password! Pleast try again')
                        .position('bottom right')
                        .hideDelay(3000)                          
                );
            }
        }
        function closeDialog(){
            $mdDialog.hide();
        }
    }

    function CustomerDialogController($scope, $mdDialog, $http, $mdToast, $rootScope, 
                                      customer, allCustomers, lightInstallers, user, lightContractors, contractorsWithInstallers) {
        var vm = this;
        vm.user = user;
        vm.allCustomers = allCustomers;
        vm.installationStatus = [
            {
                installation_status_id: 1,
                installation_status_desc: 'Created'
            },
            {
                installation_status_id: 2,
                installation_status_desc: 'Finished'
            },
            {
                installation_status_id: 3,
                installation_status_desc: 'Abandoned'
            }
        ]

        vm.installationType = [
            {
                installation_type_id: 1,
                installation_type_desc: 'Standard'
            },
            {
                installation_type_id: 2,
                installation_type_desc: 'Commercial'
            }
        ]

        vm.postProcessingStatus = [{
            post_processing_status_id: 1,
            post_processing_status_desc: 'Not Started'
        }, {
            post_processing_status_id: 2,
            post_processing_status_desc: 'Completed'
        }, {
            post_processing_status_id: 3,
            post_processing_status_desc: 'Failed'
        }]
        vm.customer = customer;
        vm.lightInstallers = lightInstallers;
        vm.lightContractors = lightContractors;
        vm.contractorsWithInstallers = contractorsWithInstallers;
        vm.base_api_url = $rootScope.base_api_url;
       
        // data
        vm.newCustomer = customer ? false : true;
        //vm.minDate = new Date(); // we don't need any mindate validation check
        vm.customerDialogHeader = customer ? "Edit" : "New Customer";
        vm.signalQualityFormatWarning = false;

        if(vm.newCustomer){
            var customer = {
                installation_status_id : 1,
                installation_type_id: 1,
                post_processing_status_id: 1
            };
            vm.customer = customer;
        }
        
        // method
        vm.addNewCustomer = addNewCustomer;
        vm.saveCustomer = saveCustomer;
        vm.closeDialog = closeDialog;
        vm.addressChanged = addressChanged;
        vm.onChangeContractorDropdown = onChangeContractorDropdown;
        vm.cancelContractorSelection = cancelContractorSelection;
        vm.cancelInstallerSelection = cancelInstallerSelection;
        vm.validateTextOnly = validateTextOnly;
        vm.validatePhone = validatePhone;
        vm.validateRouterUnit = validateRouterUnit;
        vm.validateRadioUnit = validateRadioUnit;
        vm.validateSignalQuality = validateSignalQuality;

        // customer form validation
        function validateSignalQuality(formProperty, signalQuality){
            var regex = new RegExp("^[-][0-5]{1}[0-9]{1}[/][-][0-5]{1}[0-9]{1}$");
            if(formProperty.$dirty){
                if(formProperty.$modelValue === "") vm.signalQualityFormatWarning = false;
                else if(!regex.test(signalQuality)) vm.signalQualityFormatWarning = true;
                else vm.signalQualityFormatWarning = false;
            }
        }

        function validateRadioUnit(formProperty, radioUnit){
            var regex = new RegExp("^[A-Z]{2}[-][0-9]{4}$");
            if(formProperty.$dirty){
                if(formProperty.$modelValue === "") formProperty.$setValidity('radioUnitFormat', true);
                else if(regex.test(radioUnit)) formProperty.$setValidity('radioUnitFormat', true);
                else formProperty.$setValidity('radioUnitFormat', false);
            }
        }

        function validateRouterUnit(formProperty, routerUnit){
            var regex1 = new RegExp("^RB960PGS[-][0-9]{4}$");
            var regex2 = new RegExp("^RB962AC[-][0-9]{4}$");
            var regex3 = new RegExp("^RB1100[-][0-9]{4}$");
            if(formProperty.$dirty){
                if(formProperty.$modelValue === "") formProperty.$setValidity('routerUnitFormat', true);
                else if(regex1.test(routerUnit) || regex2.test(routerUnit) || regex3.test(routerUnit)) formProperty.$setValidity('routerUnitFormat', true);
                else formProperty.$setValidity('routerUnitFormat', false);
            }
        }

        vm.formatPhoneNumber = formatPhoneNumber;

        function formatPhoneNumber(phoneNumberString) {
            var cleaned = ('' + phoneNumberString).replace(/\D/g, '');
            var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
            if (match) {
              return '(' + match[1] + ') ' + match[2] + '-' + match[3];
            }
            return null;
        }

        function validatePhone(formProperty, phone){
            var regex = new RegExp("^[0-9]{10}$");
            var regex2 = new RegExp("^[(0-9)]{5}[ ][0-9]{3}[-][0-9]{4}$");
            if(formProperty.$dirty){
                if(formProperty.$modelValue === "") formProperty.$setValidity('phoneFormat', true);
                else if(regex.test(phone) || regex2.test(phone)) {
                    formProperty.$setValidity('phoneFormat', true);
                    if(formProperty.$name === 'phone_home'){
                        vm.customer.phone_home = this.formatPhoneNumber(phone);
                    } else if (formProperty.$name === 'phone_cell'){
                        vm.customer.phone_cell = this.formatPhoneNumber(phone);
                    }
                }
                else formProperty.$setValidity('phoneFormat', false);
            }
            
        }
        function validateTextOnly(formProperty, name){
            var regex = new RegExp("^[a-zA-Z -]+$");
            if(regex.test(name)){
                formProperty.$setValidity('onlyCharacters', true);
            } else {
                formProperty.$setValidity('onlyCharacters', false);
            }
        }

        // cacnel dropdow selections
        function cancelInstallerSelection(){    
            vm.customer.installer_id = undefined;
        }

        function cancelContractorSelection(){
            vm.customer.contractor_id = undefined;
            vm.customer.installer_id = undefined;
        }

        // on change contractor dropdown change the dropdown value for installer

        function onChangeContractorDropdown(contractor_id){
            angular.forEach(vm.contractorsWithInstallers, function(item){
                if(item && item.contractor_id === contractor_id){
                    vm.lightInstallers = item.installers;
                    return;
                }
            });
        }

        // this function is not being used right now as we don't need to convert location data from address
        function addressChanged(){
            if(vm.customer.address){
                var geometry = vm.customer.address.geometry;
                vm.customer.address = vm.customer.address.formatted_address;
                vm.customer.address_lat = geometry.location.lat();
                vm.customer.address_long = geometry.location.lng();
            }   
        }

        function formateDate(s){
            var d = new Date(s);
            var date = [
                d.getFullYear(),
                ('0' + (d.getMonth() + 1)).slice(-2),
                ('0' + d.getDate()).slice(-2)
            ].join('-');
            return date;
        }

        vm.addNewCustomerService = addNewCustomerService;

        function addNewCustomerService(){
            $http({
                url: vm.base_api_url + '/api/customers/create',
                method: "POST",
                data: {
                    "first_name": vm.customer.first_name,
                    "last_name": vm.customer.last_name,
                    "phone_home": vm.customer.phone_home,
                    "phone_cell": vm.customer.phone_cell,
                    "email": vm.customer.email,
                    "address": vm.customer.address,
                    "unit_no": vm.customer.unit_no,
                    "installation_date": vm.customer.installation_date ? formateDate(vm.customer.installation_date) : null,
                    "radio_unit_no": vm.customer.radio_unit_no,
                    "router_unit_no": vm.customer.router_unit_no,
                    "elevation": vm.customer.elevation,
                    "longitude": vm.customer.longitude,
                    "latitude": vm.customer.latitude,
                    "installer_id": vm.customer.installer_id || vm.user.installer_id,
                    "contractor_id": vm.customer.contractor_id || vm.user.contractor_id,
                    "installation_status_id": vm.customer.installation_status_id,
                    "installation_type_id" : vm.customer.installation_type_id,
                    "company_name" : vm.customer.company_name || null,
                    "signal_quality" : vm.customer.signal_quality,
                    "post_processing_status_id" : vm.customer.post_processing_status_id,
                    "post_processing_note" : vm.customer.post_processing_note,
                    "address_lat": vm.customer.address.geometry.location.lat(),
                    "address_long": vm.customer.address.geometry.location.lng(),
                    "note": vm.customer.note,
                    "user": vm.user,
                    "created_by" : vm.user
                }
            }).then(function successCallback(response){
                if(response){
                    if(response.data === 'is_existing_customer'){
                        vm.isExistingCustomer = true;
                    } else {
                        $rootScope.$broadcast('customerAdded', response);
                        $mdDialog.hide();
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Customer Added Successfully')
                                .position('bottom right')
                                .hideDelay(3000)                          
                        );
                    }
                }
            }, function errorCallback(response){});
        }

        vm.isAddressExists = isAddressExists;
        function isAddressExists(addedCustomerAddress){
            var addressExists = false;
            if(vm.allCustomers && vm.allCustomers.length > 0){
                vm.allCustomers.forEach(function(customer){
                    if(customer.address === addedCustomerAddress) {
                        addressExists = true;
                        return;
                    }
                })
            }
            return addressExists;
        }

        // method implementation
        function addNewCustomer(){
            if(vm.customer.password === vm.customer.retype_password){
                if(vm.isAddressExists(vm.customer.address['formatted_address'])){
                    var confirm = $mdDialog.alert({
                        title: 'Warning!',
                        textContent: 'Just to warn you that there is already a customer existed in the system with the address you provided',
                        ok: 'Ok'
                    });
                    $mdDialog.show(confirm).then(function(){
                        vm.addNewCustomerService();
                    });
                } else {
                    vm.addNewCustomerService();
                }
            } else {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Retype password is not matching with password! Pleast try again')
                        .position('bottom right')
                        .hideDelay(3000)                          
                );
            }
            
        }

        function saveCustomer(customer){
            $http({
                url: vm.base_api_url + '/api/customers/'+ customer.customer_id,
                method: "PUT",
                data: {
                    "first_name": customer.first_name,
                    "last_name": customer.last_name,
                    "phone_home": customer.phone_home,
                    "phone_cell": customer.phone_cell,
                    "email": customer.email,
                    "address": customer.address,
                    "unit_no": customer.unit_no,
                    "installation_date": customer.installation_date ? formateDate(customer.installation_date) : null,
                    "radio_unit_no": customer.radio_unit_no,
                    "router_unit_no": customer.router_unit_no,
                    "elevation": customer.elevation,
                    "longitude": customer.longitude,
                    "latitude": customer.latitude,
                    "installer_id": customer.installer_id || vm.user.installer_id,
                    "contractor_id": customer.contractor_id || vm.user.contractor_id,
                    "installation_status_id": customer.installation_status_id,
                    "installation_type_id": customer.installation_type_id,
                    "company_name": customer.company_name || null,
                    "signal_quality": customer.signal_quality,
                    "post_processing_status_id": customer.post_processing_status_id,
                    "post_processing_note": customer.post_processing_note,
                    "address_lat": customer.address.geometry ? customer.address.geometry.location.lat() : customer.address_lat,
                    "address_long": customer.address.geometry ? customer.address.geometry.location.lng() : customer.address_long,
                    "note": customer.note,
                    "user": vm.user,
                    "created_by": vm.user
                }
            }).then(function successCallback(response){
                if(response){
                    if(response.data === 'is_existing_customer'){
                        vm.isExistingCustomer = true;
                    } else {
                        $rootScope.$broadcast('customerUpdated', response.data);
                        $mdDialog.hide();
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Customer Updated Successfully')
                                .position('bottom right')
                                .hideDelay(3000)                          
                        );
                    }
                }
            }, function errorCallback(response){});
        }

        
        function closeDialog(){
            $mdDialog.hide();
        }
    }

    function AdminDialogController($scope, $mdDialog, $http, $mdToast, $rootScope, admin) {
        var vm = this;
        vm.admin = admin;
        vm.base_api_url = $rootScope.base_api_url;
       
        // data
        vm.newAdmin = admin ? false : true;
        vm.adminDialogHeader = admin ? "Edit" : "New Admin";
        
        // method
        vm.addNewAdmin = addNewAdmin;
        vm.saveAdmin = saveAdmin;
        vm.closeDialog = closeDialog;
        vm.onChangeFirstName = onChangeFirstName;
        vm.onChangeLastName = onChangeLastName;

        // method implementation
        function onChangeFirstName(){
            vm.admin.username = vm.admin.first_name.toString().toLowerCase().charAt(0);
        }
        function onChangeLastName(){
            vm.admin.username = vm.admin.first_name.toString().toLowerCase().charAt(0) + vm.admin.last_name.toString().toLowerCase();
        }
        function addNewAdmin(){
            if(vm.admin.password === vm.admin.retype_password){
                $http({
                    url: vm.base_api_url + '/api/superAdmin/admins/create',
                    method: "POST",
                    data: {
                        "first_name": vm.admin.first_name,
                        "last_name": vm.admin.last_name,
                        "phone_office": vm.admin.phone_office,
                        "phone_cell": vm.admin.phone_cell,
                        "email": vm.admin.email,
                        "password": vm.admin.password,
                        "department": vm.admin.department,
                        "status": vm.admin.status,
                        "username": vm.admin.username
                    }
                }).then(function successCallback(response){
                    if(response){
                        if(response.data === 'is_existing_user'){
                            vm.isExistingUser = true;
                        } else {
                            $rootScope.$broadcast('adminAdded', response.data);
                        
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('Admin Added Successfully')
                                    .position('bottom right')
                                    .hideDelay(3000)                          
                            );
                            $mdDialog.hide();
                        }
                    }
                    
                    
                    
                }, function errorCallback(response){});
            } else {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Retype password is not matching with password! Pleast try again')
                        .position('bottom right')
                        .hideDelay(3000)                          
                );
            }
        }
        function saveAdmin(admin){
            if(admin.password === admin.retype_password){
                $http({
                    url: vm.base_api_url + '/api/superAdmin/admins/'+ admin.admin_id,
                    method: "PUT",
                    data: {
                        "first_name": admin.first_name,
                        "last_name": admin.last_name,
                        "phone_office": admin.phone_office,
                        "phone_cell": admin.phone_cell,
                        "email": admin.email,
                        "password": admin.password,
                        "department": admin.department,
                        "status": admin.status
                    }
                }).then(function successCallback(response){
                    if(response.data){
                        $rootScope.$broadcast('adminUpdated', response.data);
                    }
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Admin updated Successfully')
                            .position('bottom right')
                            .hideDelay(3000)                          
                    );
                    vm.closeDialog();
                }, function errorCallback(response){});
            } else {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Retype password is not matching with password! Pleast try again')
                        .position('bottom right')
                        .hideDelay(3000)                          
                );
            }
        }
        function closeDialog(){
            $mdDialog.hide();
        }
    }

})();
