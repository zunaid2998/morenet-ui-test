(function ()
{
    'use strict';

    angular
        .module('app.morenet', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.morenet', {
                url    : '/morenet/:groupId',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/morenet/morenet.html',
                        controller : 'MorenetController as vm'
                    }
                },
                params: {
                    user: null
                }
            });
    }
})();