(function ()
{
    'use strict';

    angular
        .module('app.chat')
        .controller('ChatController', ChatController);

    /** @ngInject */
    function ChatController(Contacts, ChatsService, $mdSidenav, User, $timeout, $document, $mdMedia, $state, $http, $scope, $rootScope, $mdDialog, $cookies, $mdToast, $stateParams) {
        if($cookies.get('userLoggedIn') === 'false'){
            $state.go('app.pages_auth_login');
        }

        

        var vm = this;
        vm.base_api_url = $rootScope.base_api_url;

        // Data
        vm.groupId = $stateParams.groupId || 1;
        vm.groupName = $stateParams.groupName || '';
        vm.groupAdminId = $stateParams.groupAdminId;
        vm.isMasterUser = $stateParams.isMasterUser;
        vm.contactSearch = "";
        vm.isShowContactList = false;

        // methods
        vm.getTenantsByUserId = getTenantsByUserId;
        vm.contacts = getAllSubGroupsByGroupId(vm.groupId);
        vm.allUsers = [];
        vm.chats = ChatsService.chats;
        vm.user = User.data;
        $rootScope.user_id = 3;
        vm.leftSidenavView = false;
        vm.chat = undefined;
        vm.settings = {
            chatbotMode: false
        };
        vm.recentChatSelected = false;
        vm.volunteersSelected = true;
        vm.replyInProgress = false;

        // Methods
        vm.getChat = getChat;
        vm.getGroupChat = getGroupChat;
        vm.getUserChat = getUserChat;
        vm.toggleSidenav = toggleSidenav;
        vm.toggleLeftSidenavView = toggleLeftSidenavView;
        vm.reply = reply;
        vm.setUserStatus = setUserStatus;
        vm.clearMessages = clearMessages;
        vm.chatLogoutClicked = chatLogoutClicked;
        vm.scrollToBottomOfChat = scrollToBottomOfChat;
        vm.setReplyMessageFromSuggestion = setReplyMessageFromSuggestion;
        vm.openContactDialog = openContactDialog;
        vm.editContactClicked = editContactClicked;
        vm.openBroadcastDialog =  openBroadcastDialog;
        vm.uploadContactList = uploadContactList;
        vm.backToBuildings = backToBuildings;
        vm.checkUserRole = checkUserRole;
        vm.resetReplyTextarea = resetReplyTextarea;
        vm.scrollToBottomOfChat = scrollToBottomOfChat;
        vm.onRecentChatSelected = onRecentChatSelected;
        vm.onVolunteersSelected = onVolunteersSelected;
        vm.recentChatClicked = recentChatClicked;
        vm.openNewGroupDialog = openNewGroupDialog;
        vm.openEditGroupDialog = openEditGroupDialog;
        vm.archiveSubGroup = archiveSubGroup;
        vm.goToDashboard = goToDashboard;

        vm.selectedIndex = null;
        vm.setSelectedIndex = setSelectedIndex;
        vm.toggleShowContactList = toggleShowContactList;
        vm.getAllUsersByGroupId = getAllUsersByGroupId;

        function goToDashboard(){
            $state.go('app.buildings',{
                groupAdminId : vm.groupAdminId,
                isMasterUser: vm.isMasterUser
            });
        }

        function toggleShowContactList(){
            vm.isShowContactList = !vm.isShowContactList;
            if(vm.isShowContactList){
                vm.getAllUsersByGroupId(vm.groupId);
            }
        }

        function backToBuildings(){
            $state.go('app.buildings',{
                groupAdminId : vm.groupAdminId,
                isMasterUser: vm.isMasterUser
            });
        }

        function checkUserRole(){

        }

        $scope.$on("subGroupAdded", function(evt, data){
            vm.contacts = data;
        });

        $scope.$on("tenantAdded", function(evt , data){ 
            vm.contacts = data;
        });
        $scope.$on("tenantSaved", function(evt , data){ 
            vm.contacts = data;
        });

        function setSelectedIndex(index){
            if (vm.selectedIndex === null) {
                vm.selectedIndex = index;
            }
            else if (vm.selectedIndex === index) {
                vm.selectedIndex = null;
            }
            else {
                vm.selectedIndex = index;
            }
        }
        
        

        // my changes for pusher starts from here
        

        Pusher.logToConsole = false;
        
        var pusher = new Pusher('b9c272fc75bbde51fa36', {
            cluster: 'us2',
            encrypted: false
        });
    
        var channel = pusher.subscribe('my-channel');
        channel.bind('my-event', function(data) {
            /*console.log(data);
            vm.recentChats = data.recent_chats;
            if(userInRecentChat(data)){
                data.isNewMessage = true;
            } else {
                data.isNewMessage = true;
                vm.recentChats.unshift(data);
            } */
            if(vm.chat && (vm.chat_phone_no === data.message_from || vm.chat_name === data.message_from)){
                var message = {
                    who    : 'contact',
                    message: data.message,
                    time   : new Date().toISOString()
                };
                vm.chat.push(message);
                vm.scrollToBottomOfChat();
            }
        });

        function userInRecentChat(data){
            var userInRecentChat = false;
            if(vm.recentChats.length > 0){
                vm.recentChats.forEach(function(element) {
                    if(element.message_from === data.message_from){
                        element.message = data.message;
                        userInRecentChat = true;
                    }
                });
                return userInRecentChat;
            } else {
                return false;
            }
        }

        $scope.$on("getMobileMessage", function(evt, data){
            var message = {
                who    : 'contact',
                message: data,
                time   : new Date().toISOString()
            };
            console.log(vm.chat);
            vm.chat.push(message);
            vm.customerMessage = message;
            $scope.$apply();
            scrollToBottomOfChat();
            if(!vm.settings.chatbotMode){
                $timeout(function(){
                    vm.suggestionCreated = true;
                    vm.suggestionList.forEach(function(item){
                        if(vm.customerMessage.message.toLowerCase().includes(item.type)){
                            vm.suggestions = item.suggestion;
                        }
                    });
                }, 2000);
            }
            
        });

        /* $scope.$on("isChatBotTyping", function(evt, data){
            if(data){
                var message = {
                    who    : 'user',
                    message: "Chat bot is typing ....",
                    time   : new Date().toISOString()
                };
                $timeout(function(){
                    vm.chat.push(message);
                    scrollToBottomOfChat();
                }, 2000);
                $timeout(function(){
                    var messageList  = [{
                        type: 'hello',
                        msg: 'Thank you for contacting 311, how can I help you today ?'
                    },{
                        type: 'hi',
                        msg: 'Thank you for contacting 311, how can I help you today ?'
                    },{
                        type: 'pothole', 
                        msg: 'Can you please provide the location of the pothole ?'
                    },{
                        type: 'westbound', 
                        msg: 'Thank you we have notified the Public Works dept. And a crew will be sent to inspect and fix. If you want to follow up please use ref. no 1234. Thank you for using 311.'
                    },{
                        type: 'snow', 
                        msg: 'Thank you, what is your address?'
                    },{
                        type: '123', 
                        msg: 'Sorry about the inconvenience we will send out a crew to clear it. Please contact back if no one has shown up in 24 hrs. Your ref. no. 12345'
                    },{
                        type: 'light',
                        msg: 'Thank you, we have notified the road crew, you complaint ref. no. 12345.'
                    },{
                        type: 'thanks',
                        msg: 'Welcome!!'
                    }];
                    var botMessage = 'Sorry, Bot couldn\'t find an answer!';
                    messageList.forEach(function(item){
                        if(vm.customerMessage.message.toLowerCase().includes(item.type)){
                            botMessage = item.msg;
                        }
                    });
                    var message = {
                        who    : 'user',
                        message: botMessage,
                        time   : new Date().toISOString()
                    };
                    vm.chat.pop();
                    vm.chat.push(message);
                    scrollToBottomOfChat();
                    sendMessageToServer(message);
                }, 4000)
            }
        }); */

        //////////

        /**
         * Get Chat by Contact id
         * @param contactId
         */
        function getChat(contactId){
            ChatsService.getContactChat(contactId).then(function (response)
            {
                vm.chatContactId = contactId;
                vm.chat = response.message || [];
                vm.tenant_first_name = response.tenant_first_name;
                vm.tenant_full_name = response.tenant_first_name + ' ' + response.tenant_last_name;
                vm.tenant_phone_no = response.tenant_phone_no;
                vm.tenant_email = response.tenant_email || '';
                vm.tenant_notes = response.tenant_notes || '';
                vm.tenant_apartment_no = response.tenant_apartment_no || '';

                //get chat history for this tenant
                $http({
                    url: 'http://themepacket.com/annyya-backend/api/user/+12048194224/getTenantChatHistory/'+ response.tenant_phone_no,
                    method: "GET"
                }).then(function successCallback(response){
                        console.log(response);
                        vm.chat = response.data;
                        if(response.data === 'no_chat_history_found'){
                            vm.chat = [];
                        }
                        vm.suggestionCreated = false;
                        vm.resetReplyTextarea();
                        vm.scrollToBottomOfChat();
                }, function errorCallback(response){});

                // Reset the reply textarea
                resetReplyTextarea();

                // Scroll to the last message
                scrollToBottomOfChat();

                if ( !$mdMedia('gt-md') )
                {
                    $mdSidenav('left-sidenav').close();
                }

                // Reset Left Sidenav View
                vm.toggleLeftSidenavView(false);

            });
        }

        function getGroupChat(sub_group_id, index){
            vm.setSelectedIndex(index);
            vm.selectedSubGroupId = sub_group_id;
            vm.selectedPhoneNo = undefined;
            $http({
                url: vm.base_api_url + '/api/getBroadcastHistory/'+ sub_group_id,
                method: "GET"
            }).then(function successCallback(response){
                var data = response.data;
                vm.chat = data.chatHistory;
                if(data.sub_group){
                    vm.chat_name = data.sub_group.sub_group_name;
                    vm.isBroadcastChat = true;
                }
                if(data.message === 'no_chat_history_found'){
                    vm.chat = [];
                }
                vm.suggestionCreated = false;
                vm.resetReplyTextarea();
                vm.scrollToBottomOfChat();
            }, function errorCallback(response){});
        }

        function getUserChat(user_phone_no, index){
            vm.setSelectedIndex(index)
            vm.selectedSubGroupId = undefined;
            vm.selectedPhoneNo = user_phone_no;
            $http({
                url: vm.base_api_url + '/api/getUserChatHistory',
                method: "POST",
                data: {
                    "user_phone_no": user_phone_no,
                    "group_id": vm.groupId
                }
            }).then(function successCallback(response){
                    var data = response.data;
                    vm.chat = data.chatHistory;
                    if(data.user){
                        if(data.user.user_id){
                            vm.chat_name = data.user.user_name; 
                            vm.chat_phone_no = data.user.user_phone_no;   
                        } else {
                            vm.chat_name = data.user;
                        }
                        vm.isBroadcastChat = false;
                    }
                    if(data.message === 'no_chat_history_found'){
                        vm.chat = [];
                    }
                    vm.suggestionCreated = false;
                    vm.resetReplyTextarea();
                    vm.scrollToBottomOfChat();
            }, function errorCallback(response){});
        }

        function recentChatClicked(phone_number){
            $http({
                url: vm.base_api_url + '/api/user/+12048194224/getTenantChatHistory/'+ phone_number,
                method: "GET"
            }).then(function successCallback(response){
                    console.log(response);
                    vm.chat = response.data;
                    vm.tenant_full_name = response.tenant_phone_no || '';
                    vm.resetReplyTextarea();
                    vm.scrollToBottomOfChat();
            }, function errorCallback(response){
                    console.log(response);
            });
        }

        /**
         * setReplyMessageFromSuggestion
         */

         function setReplyMessageFromSuggestion(message){
            vm.replyMessage = message;
         }

        /**
         * Reply
         */
        function reply($event)
        {
            
            // If "shift + enter" pressed, grow the reply textarea
            if ( $event && $event.keyCode === 13 && $event.shiftKey )
            {
                vm.textareaGrow = true;
                return;
            }

            // Prevent the reply() for key presses rather than the"enter" key.
            if ( $event && $event.keyCode !== 13 )
            {
                return;
            }

            // Check for empty messages
            if ( vm.replyMessage === '' )
            {
                resetReplyTextarea();
                return;
            }
            vm.replyInProgress = true;
            if(vm.selectedSubGroupId){
                console.log(vm.selectedSubGroupId);
                var broadcast_message_body = vm.replyMessage;
                var group_id = vm.groupId;
                var sub_groups = [{
                    "sub_group_id": vm.selectedSubGroupId
                }];
                $http({
                    url: vm.base_api_url + '/api/sendBroadcastMessage',
                    method: "POST",
                    data: {
                        "sub_groups": sub_groups,
                        "group_id": group_id,
                        "broadcast_message_body": broadcast_message_body
                    }
                }).then(function successCallback(response){
                        if(response.data && response.data["message"] === "broadcast_message_success"){
                            $mdToast.show(
                                $mdToast.simple()
                                .textContent('Broadcast message sent to all users')
                                .position('top right')
                                .hideDelay(3000)                         
                            );
                            resetReplyTextarea();
                            scrollToBottomOfChat();
                            vm.replyInProgress = false;
                        }
                }, function errorCallback(response){
                    vm.replyInProgress = false;
                });

                var message = {
                    who    : 'user',
                    message: vm.replyMessage,
                    time   : new Date().toISOString()
                };
                vm.chat.push(message);
                return;
            } else {
                $http({
                    url: vm.base_api_url + '/api/sendOneMessage',
                    method: "POST",
                    data: {
                        "group_id": vm.groupId,
                        "message_to": vm.selectedPhoneNo,
                        "message_body": vm.replyMessage
                    }
                }).then(function successCallback(response){
                        console.log(response);
                        vm.suggestionCreated = false;
                        vm.resetReplyTextarea();
                        vm.scrollToBottomOfChat();
                        vm.replyInProgress = false;
                }, function errorCallback(response){});
                
                var message = {
                    who    : 'user',
                    message: vm.replyMessage,
                    time   : new Date().toISOString()
                };
                vm.chat.push(message);
            }

        }

        /**
         * Call the api to send message to mobile
         */

         function sendMessageToServer(message){
            console.log('sending message to server');
            var message = message || {};
            var data = {
                message_from : '+12048194224',
                message_to : vm.tenant_phone_no,
                message_body : message.message
            }
            
            $http({
                url: vm.base_api_url + '/api/sendOneMessage',
                method: "POST",
                data: {
                    "message_from": message_from,
                    "message_to": message_to,
                    "message_body": message.message
                }
            }).then(function successCallback(response){
                    console.log(response);
                    vm.suggestionCreated = false;
                    vm.resetReplyTextarea();
                    vm.scrollToBottomOfChat();
            }, function errorCallback(response){});
            
         }

        /**
         * Clear Chat Messages
         */
        function clearMessages(ev)
        {
            var confirm = $mdDialog.confirm()
                .title('Are you sure you want to delete the chat history')
                .htmlContent('Chat history of <b>' + vm.tenant_full_name + '</b>' + ' will be deleted.')
                .ariaLabel('delete contact')
                .targetEvent(ev)
                .ok('OK')
                .cancel('CANCEL');

            $mdDialog.show(confirm).then(function (){
                $http({
                    url: 'http://themepacket.com/annyya-backend/api/user/+12048194224/deleteTenantChatHistory/'+ vm.tenant_phone_no,
                    method: "DELETE"
                }).then(function successCallback(response){
                        if(response.data === 'chat_history_deleted'){
                            vm.chat = [];
                            $mdToast.show(
                                $mdToast.simple()
                                  .textContent('Deleted Chat history of this tenant')
                                  .position('top right')
                                  .hideDelay(3000)                          
                                );
                        } else if(response.data === 'no_chat_history_found'){
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('No chat history found')
                                    .position('top right')
                                    .hideDelay(3000)                          
                                );
                        }
                        
                        vm.resetReplyTextarea();
                        vm.scrollToBottomOfChat();
                        
                }, function errorCallback(response){
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Something went wrong. please try again later')
                            .position('top right')
                            .hideDelay(3000)                          
                        );
                });
            });   
        }

        /**
         * Reset reply textarea
         */
        function resetReplyTextarea()
        {
            vm.replyMessage = '';
            vm.textareaGrow = false;
        }

        /**
         * Scroll Chat Content to the bottom
         * @param speed
         */
        function scrollToBottomOfChat()
        {
            $timeout(function ()
            {
                var chatContent = angular.element($document.find('#chat-content'));

                chatContent.animate({
                    scrollTop: chatContent[0].scrollHeight
                }, 400);
            }, 0);

        }

        /**
         * Set User Status
         */
        function setUserStatus(status)
        {
            vm.user.status = status;
        }

        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId)
        {
            $mdSidenav(sidenavId).toggle();
        }

        /**
         * Toggle Left Sidenav View
         *
         * @param view id
         */
        function toggleLeftSidenavView(id)
        {
            vm.leftSidenavView = id;
        }

        /**
         * Array prototype
         *
         * Get by id
         *
         * @param value
         * @returns {T}
         */
        Array.prototype.getById = function (value)
        {
            return this.filter(function (x)
            {
                return x.id === value;
            })[0];
        };

        // chat logout button clicked
        function chatLogoutClicked(){
            $cookies.put('userLoggedIn', 'false');
            $state.go('app.pages_auth_login');
        }

        /**
         * Open new broadcast dialog
         *
         * @param ev
         * @param contact
         */
        function openBroadcastDialog(ev){
            $mdDialog.show({
                controller         : 'BroadcastDialogController',
                controllerAs       : 'vm',
                templateUrl        : 'app/main/apps/chat/dialogs/broadcast/broadcast-dialog.html',
                parent             : angular.element($document.find('#content-container')),
                targetEvent        : ev,
                clickOutsideToClose: true,
                locals             : {
                    groupId: vm.groupId
                }
            });
        }

        /**
         * Open new contact dialog
         *
         * @param ev
         * @param contact
         */
        function openContactDialog(ev, contact){
            $mdDialog.show({
                controller         : 'ContactDialogController',
                controllerAs       : 'vm',
                templateUrl        : 'app/main/apps/chat/dialogs/contact/contact-dialog.html',
                parent             : angular.element($document.find('#content-container')),
                targetEvent        : ev,
                clickOutsideToClose: true,
                locals             : {
                    Contact : contact,
                    User    : vm.user,
                    SubGroups: vm.contacts,
                    groupId: vm.groupId
                }
            });
        }

        /**
         * Get all the contacts
         */

        function getTenantsByUserId(user_id){
            $http({
                url: 'http://themepacket.com/annyya-backend/api/getTenantsByUserId/' + user_id,
                method: "GET"
            }).then(function successCallback(response){
                    console.log(response);
                    if(response.data){
                        vm.contacts = response.data;
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                              .textContent('Something went wrong! Tenant lists couldn\'t retrieved from database')
                              .position('top right')
                              .hideDelay(3000)                          
                            );
                    }
                    
            }, function errorCallback(response){
                $mdToast.show(
                    $mdToast.simple()
                      .textContent('Something went wrong! Pleast try again')
                      .position('top right')
                      .hideDelay(3000)                          
                  );
            });
         }

         /**
          * Edit contact button clicked
          */

          function editContactClicked(contact) {
              // console.log(contact);
              // $rootScope.$broadcast("editContactClicked", contact);
              //vm.openContactDialog()
          }

          function onRecentChatSelected(){
              vm.recentChatSelected = true;
              vm.volunteersSelected = false;
          }

          function onVolunteersSelected(){
              vm.volunteersSelected = true;
              vm.recentChatSelected = false;
          }

          function openEditGroupDialog(ev, groupId, subGroupId){
            $mdDialog.show({
                locals: {subGroupId: subGroupId, groupId: groupId},
                controller: NewGroupDialogController,
                controllerAs: 'vm',
                templateUrl: 'app/main/apps/chat/new-group-dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen: false // Only for -xs, -sm breakpoints.
              });
          }

          function openNewGroupDialog(ev){
            $mdDialog.show({
                locals: {groupId: vm.groupId, subGroupId: null},
                controller: NewGroupDialogController,
                controllerAs: 'vm',
                templateUrl: 'app/main/apps/chat/new-group-dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen: false // Only for -xs, -sm breakpoints.
              });
          }
          function getAllSubGroupsByGroupId(groupId){
            $http({
                url: vm.base_api_url + '/api/getAllSubGroupsByGroupId/' + groupId,
                method: "GET",
            }).then(function successCallback(response){
                    if(response.data){
                        vm.contacts = response.data;
                    }
                    
            },function errorCallback(response){});
          }
          function getAllUsersByGroupId(groupId){
            $http({
                url: vm.base_api_url + '/api/getAllUsersByGroupId/' + groupId,
                method: "GET",
            }).then(function successCallback(response){
                    if(response.data){
                        vm.allUsers = response.data;
                    }
                    
            },function errorCallback(response){});
          }

          function archiveSubGroup(ev, groupId, subGroupId){
            var confirm = $mdDialog.confirm()
            .title('Are you sure want to archive this group?')
            .htmlContent('This group will be archived.')
            .ariaLabel('delete group')
            .targetEvent(ev)
            .ok('OK')
            .cancel('CANCEL');

            $mdDialog.show(confirm).then(function (){
                $http({
                    url: vm.base_api_url + '/api/archiveSubGroup/' + groupId + '/' + subGroupId,
                    method: "PUT",
                }).then(function successCallback(response){
                        if(response.data){
                            if(response.data.message === 'archive_successful'){
                                vm.contacts = response.data.sub_groups;
                                $mdToast.show(
                                    $mdToast.simple()
                                      .textContent('Group successfully archived')
                                      .position('top right')
                                      .hideDelay(3000)                          
                                  );
                            }
                        }
                        
                },function errorCallback(response){});
            });
          }
          function uploadContactList(ev){
            $mdDialog.show({
                controller         :  UploadContactsDialogController,
                controllerAs       : 'vm',
                templateUrl        : 'app/main/apps/chat/upload-contacts-dialog.html',
                parent             : angular.element($document.find('#content-container')),
                targetEvent        : ev,
                clickOutsideToClose: true,
                locals             : {
                    groupId: vm.groupId,
                    SubGroups: vm.contacts
                }
            });
          }
          
    }

    function NewGroupDialogController($scope, $mdDialog, $http, groupId, $mdToast, subGroupId, $rootScope){
        var vm = this;
        
        vm.base_api_url = $rootScope.base_api_url;

        // data
        vm.title = subGroupId ? "Edit group" : "Create New Group";
        vm.createSubGroupButton = subGroupId ? false : true;
        vm.groupId = groupId;
        vm.subGroupId = subGroupId;
        vm.users = [];
        vm.selectedUsers = [];
        vm.allSelected = false;

        // method declaration
        vm.closeDialog = closeDialog;
        vm.userCheckboxClicked = userCheckboxClicked;
        vm.selectedUserRemoved = selectedUserRemoved;
        vm.allSelectedChange = allSelectedChange;
        vm.createNewSubGroup = createNewSubGroup;
        vm.updateSubGroup = updateSubGroup;
        if(vm.subGroupId){
            getAllSelectedUsersBySubGroupId(vm.groupId, vm.subGroupId);
        } else {
            getAllUsersByGroupId(vm.groupId);
        }
        

        // method implementation
        function closeDialog(){
            $mdDialog.hide();
        }
        function allSelectedChange(){
            if(vm.allSelected){
                if(vm.selectedUsers.length > 0){
                    vm.selectedUsers = [];
                }
                vm.users.forEach(function(element) {
                    element.checked = true;
                    userCheckboxClicked(element);
                });
            } else {
                vm.users.forEach(function(element) {
                    element.checked = false;
                    userCheckboxClicked(element);
                });
            }
        }
        function selectedUserRemoved(user){
            vm.users.forEach(function(element) {
                if(element.user_id === user.user_id){
                    element.checked = false;
                }
            });
        }
        function userCheckboxClicked(user){
            if(user.checked){
                var checkCount = 0;
                vm.users.forEach(function(element){
                    if(element.checked){
                        checkCount++;
                    }
                });
                if(checkCount == vm.users.length){
                    vm.allSelected = true;
                }
                vm.selectedUsers.push(user);
            } else {
                vm.allSelected = false;
                vm.selectedUsers.forEach(function(element) {
                    if(element.user_id === user.user_id){
                        var index = vm.selectedUsers.indexOf(element);
                        vm.selectedUsers.splice(index, 1);
                    }
                });
            }
        }
        function getAllUsersByGroupId(groupId){
            $http({
                url: vm.base_api_url + '/api/getAllUsersByGroupId/' + groupId,
                method: "GET",
            }).then(function successCallback(response){
                    if(response.data){
                        vm.users = response.data;
                    }
                    
            },function errorCallback(response){});
        }
        function getAllSelectedUsersBySubGroupId(groupId, subGroupId){
            $http({
                url: vm.base_api_url + '/api/getAllSelectedUsersBySubGroupId/' + groupId + '/' + subGroupId,
                method: "GET",
            }).then(function successCallback(response){
                    if(response.data){
                        vm.users = response.data;
                        vm.users.forEach(function(user) {
                            if(user.checked){
                                vm.selectedUsers.push(user);
                                vm.subGroupName = user.sub_group_name;
                            }
                        });
                    }
                    
            },function errorCallback(response){});
        }
        function createNewSubGroup(){
            if(vm.selectedUsers.length < 1){
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Please select at least one user !')
                        .position('bottom right')
                        .hideDelay(3000)                          
                    );
                return;
            }
            $http({
                url: vm.base_api_url + '/api/createNewSubGroup',
                method: "POST",
                data: {
                    "sub_group_name": vm.subGroupName,
                    "group_id": vm.groupId ,
                    "selected_users" : vm.selectedUsers
                }
            }).then(function successCallback(response){
                console.log(response.data);
                if(response.data && response.data.message === "sub_group_created"){
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Group Created!')
                            .position('bottom right')
                            .hideDelay(3000)                          
                    );
                    $rootScope.$broadcast('subGroupAdded', response.data.groups);
                    $mdDialog.hide();
                }
                
            }, function errorCallback(response){});
        }
        function updateSubGroup(){
            if(vm.selectedUsers.length < 1){
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Please select at least one user !')
                        .position('bottom right')
                        .hideDelay(3000)                          
                    );
                return;
            }
            $http({
                url: vm.base_api_url + '/api/updateSubGroup/' + vm.subGroupId,
                method: "PUT",
                data: {
                    "sub_group_name": vm.subGroupName,
                    "group_id": vm.groupId ,
                    "selected_users" : vm.selectedUsers
                }
            }).then(function successCallback(response){
                if(response.data && response.data.message === "sub_group_saved"){
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Group Saved!')
                            .position('bottom right')
                            .hideDelay(3000)                          
                    );
                    $mdDialog.hide();
                }
                
            }, function errorCallback(response){});
        }
    }
    function ArchivedGroupController($scope, $mdDialog, $http, groupId, $mdToast, $rootScope){
        var vm = this;
        vm.base_api_url = $rootScope.base_api_url;
        // data

        // method

        // method implementation
    }
    function UploadContactsDialogController($scope, $mdDialog, $http, groupId, $mdToast, $rootScope, SubGroups){
        var vm = this;
        vm.SubGroups = SubGroups;
        vm.base_api_url = $rootScope.base_api_url;

        // data
        vm.groupId = groupId;
        $scope.csv = {
            content: null,
            header: true,
            separator: ',',
            result: null
        };

        // method
        vm.closeDialog = closeDialog;
        vm.subGroupSelected = subGroupSelected;
        vm.createNewSubGroupByUploading = createNewSubGroupByUploading;

        // method implementation
        function subGroupSelected(){
            if(vm.sub_group.sub_group_id && vm.sub_group.sub_group_id.length > 0){
                vm.sub_group.sub_group_name = "";
                vm.sub_group_name_disabled = true;
            } else {
                vm.sub_group_name_disabled = false;
            }
        }
        function closeDialog(){
            $mdDialog.hide();
        }
        function createNewSubGroupByUploading(){
            $http({
                url: vm.base_api_url + '/api/createNewSubGroupByUploading',
                method: "POST",
                data: {
                    "uploaded_users": $scope.csv.result,
                    "group_id": vm.groupId,
                    "sub_group_id": vm.sub_group.sub_group_id || null,
                    "sub_group_name": vm.sub_group.sub_group_name
                }
            }).then(function successCallback(response){
                    if(response.data){
                        $mdToast.show(
                            $mdToast.simple()
                              .textContent('Tenant uploaded successfully')
                              .position('top right')
                              .hideDelay(3000)                          
                            );
                        $rootScope.$broadcast('subGroupAdded', response.data);
                        closeDialog();
                    }
                    
            }, function errorCallback(response){
                $mdToast.show(
                    $mdToast.simple()
                      .textContent('Something went wrong! Pleast try again')
                      .position('top right')
                      .hideDelay(3000)                          
                  );
            });
        }
    }

})();