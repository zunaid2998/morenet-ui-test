(function ()
{
    'use strict';

    angular
        .module('app.chat')
        .controller('ContactDialogController', ContactDialogController);

    /** @ngInject */
    function ContactDialogController($mdDialog, Contact, SubGroups, User, msUtils, $mdToast, $http, $scope, $rootScope, groupId)
    {
        var vm = this;
        vm.base_api_url = $rootScope.base_api_url;
        // Data
        vm.title = 'Edit Contact';
        vm.contact = angular.copy(Contact);
        vm.SubGroups = SubGroups;
        vm.groupId = groupId;
        vm.newContact = false;
        vm.allFields = false;

        $rootScope.$on('editContactClicked', function(evt, data){
            console.log(data);
            vm.contact = data;

        })

        if ( !vm.contact )
        {
            vm.contact = {
                'id'      : msUtils.guidGenerator(),
                'name'    : '',
                'lastName': '',
                'avatar'  : 'assets/images/avatars/profile.jpg',
                'nickname': '',
                'company' : '',
                'jobTitle': '',
                'email'   : '',
                'phone'   : '',
                'address' : '',
                'birthday': null,
                'notes'   : ''
            };

            vm.title = 'New Contact';
            vm.newContact = true;
            vm.contact.tags = [];
        }

        // Methods
        vm.addNewContact = addNewContact;
        vm.saveContact = saveContact;
        vm.deleteContactConfirm = deleteContactConfirm;
        vm.closeDialog = closeDialog;
        vm.toggleInArray = msUtils.toggleInArray;
        vm.exists = msUtils.exists;

        

        //////////

        /**
         * Add new contact
         */
        function addNewContact()
        {
            $http({
                url: vm.base_api_url + '/api/createNewUser',
                method: "POST",
                data: {
                    "user_name" : vm.contact.tenant_first_name,
                    "user_phone_no" : vm.contact.tenant_phone_no,
                    "user_notes" : vm.contact.tenant_notes,
                    "group_id" : vm.groupId,
                    "sub_group_id": vm.contact.sub_group_id
                }
            }).then(function successCallback(response){
                    if(response.data){
                        $mdToast.show(
                            $mdToast.simple()
                              .textContent('Tenant added successfully')
                              .position('top right')
                              .hideDelay(3000)                          
                            );
                        // $rootScope.$broadcast('tenantAdded', response.data);
                        closeDialog();
                    }
                    
            }, function errorCallback(response){
                $mdToast.show(
                    $mdToast.simple()
                      .textContent('Something went wrong! Pleast try again')
                      .position('top right')
                      .hideDelay(3000)                          
                  );
            });
            
        }

        /**
         * Save contact
         */
        function saveContact(contact)
        {
            $http({
                url: 'http://themepacket.com/annyya-backend/api/updateTenant/' + contact.tenant_id,
                method: "PUT",
                data: {
                    "tenant_first_name" : vm.contact.tenant_first_name,
                    "tenant_last_name" : vm.contact.tenant_last_name,
                    "tenant_email" : vm.contact.tenant_email,
                    "tenant_phone_no" : vm.contact.tenant_phone_no,
                    "user_id" : vm.groupId,
                    "tenant_apartment_no" :  vm.contact.tenant_apartment_no,
                    "tenant_notes" : vm.contact.tenant_notes
                }
            }).then(function successCallback(response){
                    console.log(response);
                    if(response.data){
                        $mdToast.show(
                            $mdToast.simple()
                              .textContent('Tenant saved successfully')
                              .position('top right')
                              .hideDelay(3000)                          
                            );
                        $rootScope.$broadcast('tenantSaved', response.data);
                        closeDialog();
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                              .textContent('Something went wrong! Pleast try again')
                              .position('top right')
                              .hideDelay(3000)                          
                            );
                    }
                    
            }, function errorCallback(response){
                $mdToast.show(
                    $mdToast.simple()
                      .textContent('Something went wrong! Pleast try again')
                      .position('top right')
                      .hideDelay(3000)                          
                  );
            });
        }

        /**
         * Delete Contact Confirm Dialog
         */
        function deleteContactConfirm(ev)
        {
            var confirm = $mdDialog.confirm()
                .title('Are you sure want to delete the contact?')
                .htmlContent('<b>' + vm.contact.tenant_first_name + ' ' + vm.contact.tenant_last_name + '</b>' + ' will be deleted.')
                .ariaLabel('delete contact')
                .targetEvent(ev)
                .ok('OK')
                .cancel('CANCEL');

            $mdDialog.show(confirm).then(function ()
            {
                $http({
                    url: 'http://themepacket.com/annyya-backend/api/user/'+ vm.groupId + '/deleteTenant/' + vm.contact.tenant_id,
                    method: "DELETE"
                }).then(function successCallback(response){
                        console.log(response);
                        if(response.data){
                            $mdToast.show(
                                $mdToast.simple()
                                  .textContent('Tenant Deleted successfully')
                                  .position('top right')
                                  .hideDelay(3000)                          
                                );
                            $rootScope.$broadcast('tenantSaved', response.data);
                            closeDialog();
                        } else {
                            $mdToast.show(
                                $mdToast.simple()
                                  .textContent('Something went wrong! Pleast try again')
                                  .position('top right')
                                  .hideDelay(3000)                          
                                );
                        }
                        
                }, function errorCallback(response){
                    $mdToast.show(
                        $mdToast.simple()
                          .textContent('Something went wrong! Pleast try again')
                          .position('top right')
                          .hideDelay(3000)                          
                      );
                });

            });
        }

        /**
         * Close dialog
         */
        function closeDialog()
        {
            $mdDialog.hide();
        }

    }
})();