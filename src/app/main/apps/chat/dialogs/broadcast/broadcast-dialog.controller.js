(function ()
{
    'use strict';

    angular
        .module('app.chat')
        .controller('BroadcastDialogController', BroadcastDialogController);

    /** @ngInject */
    function BroadcastDialogController($mdDialog, msUtils, $mdToast, $http, $scope, $rootScope, groupId)
    {
        var vm = this;
        vm.base_api_url = $rootScope.base_api_url;
        // Data
        vm.title = 'Broadcast message';
        vm.newContact = false;
        vm.allFields = false;
        vm.contacts = [];
        vm.selectedSubGroups = [];
        vm.groupId = groupId;

        

        $rootScope.$on('editContactClicked', function(evt, data){
            console.log(data);
            vm.contact = data;

        })

        // Methods
        vm.addNewContact = addNewContact;
        vm.saveContact = saveContact;
        vm.deleteContactConfirm = deleteContactConfirm;
        vm.closeDialog = closeDialog;
        vm.toggleInArray = msUtils.toggleInArray;
        vm.exists = msUtils.exists;
        vm.sendBroadcastMessage = sendBroadcastMessage;
        vm.sendBroadcastMessageToServer = sendBroadcastMessageToServer;
        vm.getTenantsByUserId  = getTenantsByUserId;
        vm.getAllSubGroupsByGroupId = getAllSubGroupsByGroupId;
        vm.subGroupCheckboxClicked = subGroupCheckboxClicked;
        vm.getAllSubGroupsByGroupId(vm.groupId);
        

        function sendBroadcastMessage(){
            if(vm.contacts && vm.contacts.length > 0) {
                var broadcast_message_body = vm.broadcast_message;
                var group_id = vm.groupId;
                var sub_groups = [];
                vm.contacts.forEach(function(contact){
                    sub_groups.push({
                        "sub_group_id": contact.sub_group_id
                    });
                });
                $http({
                    url: vm.base_api_url + '/api/sendBroadcastMessage',
                    method: "POST",
                    data: {
                        "sub_groups": sub_groups,
                        "group_id": group_id,
                        "broadcast_message_body": broadcast_message_body
                    }
                }).then(function successCallback(response){
                        if(response.data && response.data["message"] === "broadcast_message_success"){
                            $mdToast.show(
                                $mdToast.simple()
                                .textContent('Broadcast message sent to all users')
                                .position('top right')
                                .hideDelay(3000)                         
                            );
                            
                        }
                        closeDialog();
                }, function errorCallback(response){});
            } else {
                $mdToast.show(
                    $mdToast.simple()
                    .textContent('You need to select at least one group to send broadcast message')
                    .position('top right')
                    .hideDelay(3000)                         
                );
            }
        }

        /**
         * 
         * Send broadcast message to server. 
         */

        function sendBroadcastMessageToServer(message, phone_no){
            console.log('sending message to server');
            var message = message || {};
            var data = {
                message_from : '+12048194224',
                message_to : phone_no,
                message_body : message
            }
            
            $http({
                url: 'http://themepacket.com/annyya-backend/api/sendOneMessage/'+ data.message_from + '/' + data.message_to + '/' + data.message_body,
                method: "GET"
            }).then(function successCallback(response){
                    console.log(response);
            }, function errorCallback(response){
                    console.log(response);
            });
         }

        //////////

        /**
         * Add new contact
         */
        function addNewContact()
        {
            $http({
                url: 'http://themepacket.com/annyya-backend/api/storeTenant',
                method: "POST",
                data: {
                    "tenant_first_name" : vm.contact.tenant_first_name,
                    "tenant_last_name" : vm.contact.tenant_last_name,
                    "tenant_email" : vm.contact.tenant_email,
                    "tenant_phone_no" : vm.contact.tenant_phone_no,
                    "user_id" : 3,
                    "tenant_apartment_no" :  vm.contact.tenant_apartment_no,
                    "tenant_notes" : vm.contact.tenant_notes
                }
            }).then(function successCallback(response){
                    console.log(response);
                    if(response.data){
                        $mdToast.show(
                            $mdToast.simple()
                              .textContent('Tenant added successfully')
                              .position('top right')
                              .hideDelay(3000)                          
                            );
                        $rootScope.$broadcast('tenantAdded', response.data);
                        closeDialog();
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                              .textContent('Something went wrong! Pleast try again')
                              .position('top right')
                              .hideDelay(3000)                          
                            );
                    }
                    
            }, function errorCallback(response){
                $mdToast.show(
                    $mdToast.simple()
                      .textContent('Something went wrong! Pleast try again')
                      .position('top right')
                      .hideDelay(3000)                          
                  );
            });
            
        }

        /**
         * Save contact
         */
        function saveContact(contact)
        {
            $http({
                url: 'http://themepacket.com/annyya-backend/api/updateTenant/' + contact.tenant_id,
                method: "PUT",
                data: {
                    "tenant_first_name" : vm.contact.tenant_first_name,
                    "tenant_last_name" : vm.contact.tenant_last_name,
                    "tenant_email" : vm.contact.tenant_email,
                    "tenant_phone_no" : vm.contact.tenant_phone_no,
                    "tenant_apartment_no" :  vm.contact.tenant_apartment_no,
                    "tenant_notes" : vm.contact.tenant_notes
                }
            }).then(function successCallback(response){
                    console.log(response);
                    if(response.data){
                        $mdToast.show(
                            $mdToast.simple()
                              .textContent('Tenant saved successfully')
                              .position('top right')
                              .hideDelay(3000)                          
                            );
                        $rootScope.$broadcast('tenantSaved', response.data);
                        closeDialog();
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                              .textContent('Something went wrong! Pleast try again')
                              .position('top right')
                              .hideDelay(3000)                          
                            );
                    }
                    
            }, function errorCallback(response){
                $mdToast.show(
                    $mdToast.simple()
                      .textContent('Something went wrong! Pleast try again')
                      .position('top right')
                      .hideDelay(3000)                          
                  );
            });
        }

        /**
         * Delete Contact Confirm Dialog
         */
        function deleteContactConfirm(ev)
        {
            var confirm = $mdDialog.confirm()
                .title('Are you sure want to delete the contact?')
                .htmlContent('<b>' + vm.contact.tenant_first_name + ' ' + vm.contact.tenant_last_name + '</b>' + ' will be deleted.')
                .ariaLabel('delete contact')
                .targetEvent(ev)
                .ok('OK')
                .cancel('CANCEL');

            $mdDialog.show(confirm).then(function ()
            {
                $http({
                    url: 'http://themepacket.com/annyya-backend/api/deleteTenant/' + vm.contact.tenant_id,
                    method: "DELETE"
                }).then(function successCallback(response){
                        console.log(response);
                        if(response.data){
                            $mdToast.show(
                                $mdToast.simple()
                                  .textContent('Tenant Deleted successfully')
                                  .position('top right')
                                  .hideDelay(3000)                          
                                );
                            $rootScope.$broadcast('tenantSaved', response.data);
                            closeDialog();
                        } else {
                            $mdToast.show(
                                $mdToast.simple()
                                  .textContent('Something went wrong! Pleast try again')
                                  .position('top right')
                                  .hideDelay(3000)                          
                                );
                        }
                        
                }, function errorCallback(response){
                    $mdToast.show(
                        $mdToast.simple()
                          .textContent('Something went wrong! Pleast try again')
                          .position('top right')
                          .hideDelay(3000)                          
                      );
                });

            });
        }

        function closeDialog(){
            $mdDialog.hide();
        }
        function getTenantsByUserId(user_id){
            $http({
                url: 'http://themepacket.com/annyya-backend/api/getTenantsByUserId/' + user_id,
                method: "GET"
            }).then(function successCallback(response){
                    console.log(response);
                    if(response.data && response.data.length > 0){
                        response.data.forEach(function(element) {
                            vm.contacts.push(element.tenant_phone_no);
                        });
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                              .textContent('Something went wrong! Tenant lists couldn\'t retrieved from database')
                              .position('top right')
                              .hideDelay(3000)                          
                            );
                    }
                    
            }, function errorCallback(response){
                $mdToast.show(
                    $mdToast.simple()
                      .textContent('Something went wrong! Pleast try again')
                      .position('top right')
                      .hideDelay(3000)                          
                  );
            });
        }
        function getAllSubGroupsByGroupId(groupId){
            $http({
                url: vm.base_api_url + '/api/getAllSubGroupsByGroupId/' + groupId,
                method: "GET"
            }).then(function successCallback(response){
                    if(response.data){
                       vm.contacts = response.data;
                    }  
            }, function errorCallback(response){});
        }
        function subGroupCheckboxClicked(subGroup){
            if(subGroup.checked){
                vm.selectedSubGroups.push(subGroup);
            } else {
                vm.selectedSubGroups.forEach(function(element) {
                    if(element.sub_group_id === subGroup.sub_group_id){
                        var index = vm.selectedSubGroups.indexOf(element);
                        vm.selectedSubGroups.splice(index, 1);
                    }
                });
            }
            console.log(vm.selectedSubGroups);
        }

    }
})();