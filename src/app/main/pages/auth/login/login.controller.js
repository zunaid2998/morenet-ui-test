(function ()
{
    'use strict';

    angular
        .module('app.pages.auth.login')
        .controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController($state, $http, $mdToast, $cookies, $rootScope)
    {
        // Data
        var vm = this;
        vm.loginClicked = loginClicked;
        vm.authenticUser = false;
        vm.base_api_url = $rootScope.base_api_url;

        // Methods
        function loginClicked(){
            $http({
                url: vm.base_api_url + '/api/userAuthenticate',
                method: "POST",
                data: {
                    "username" : vm.form.email,
                    "password" : vm.form.password
                }
            }).then(function successCallback(response){
                    if(response && response.data === 'user_not_found'){
                        $mdToast.show(
                            $mdToast.simple()
                              .textContent('Wrong username or password. Please try again')
                              .position('top right')
                              .hideDelay(3000)                          
                          );
                        vm.authenticUser = false;
                    } else if(response && response.data === 'user_inactive'){
                        $mdToast.show(
                            $mdToast.simple()
                              .textContent('The user is not active. Please contact your administrator to make it back to active mode')
                              .position('top right')
                              .hideDelay(3000)                          
                          );
                        vm.authenticUser = false;
                    } else if(response){
                        $state.go('app.morenet', {
                            user : response.data
                        });
                    }
                    
            }, function errorCallback(response){
                $mdToast.show(
                    $mdToast.simple()
                      .textContent('Something went wrong. Please try again later')
                      .position('top right')
                      .hideDelay(3000)                          
                  );
            });
        }

        //////////
    }
})();